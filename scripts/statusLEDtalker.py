#!/usr/bin/env python

import rospy
from rees.msg import mStatusLED

def talker():
    pub = rospy.Publisher('mStatusLED', mStatusLED, queue_size=10)
    rospy.init_node('StatusLEDtalker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        msgStatusLED = mStatusLED()
        msgStatusLED.enabe = True
        msgStatusLED.data = True
        #rospy.loginfo(hello_str)
        pub.publish(msgStatusLED)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
