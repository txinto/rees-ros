#!/usr/bin/env python

import rospy
from rees.msg import mServoAlarm

def talker():
    pub = rospy.Publisher('ServoAlarm', mServoAlarm, queue_size=10)
    rospy.init_node('ServoAlarmtalker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        msgServoAlarm = mServoAlarm()
        msgServoAlarm.enabe = True
        msgServoAlarm.data = True
        #rospy.loginfo(hello_str)
        pub.publish(msgServoAlarm)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
