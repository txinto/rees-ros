#!/usr/bin/env python

import rospy
from rees.msg import mStatusLED
from rees.msg import mServoAlarm

def mStatusLEDcb(data):
    rospy.loginfo('statusLED =  %d', data.data)

def mServoAlarmcb(data):
    rospy.loginfo('ServoAlarm =  %d', data.data)

def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber('statusLED', mStatusLED, mStatusLEDcb)
    rospy.Subscriber('ServoAlarm', mServoAlarm, mServoAlarmcb)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
