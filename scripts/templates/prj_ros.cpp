#include <prj_cfg.h>
#ifdef CFG_USE_ROSSERIAL
#include <Arduino.h>
#include <ros.h>

$includes_t

$includes_m

#include <DRE.h>
#include <extra_DRE.h>
#include <prj_DRE.h>

extern t_dre dre;
extern t_diag diag;
extern t_extra_DRE edre;
extern t_prj_DRE pdre;
extern t_prj_diag pdiag;

/* Messages */
$msgs

/* ROS handler */
ros::NodeHandle  nh;

/* Callbacks */
$callbacks

/* PubSub */
$suscribers

$publishers

/* Setup */
void prj_ros_setup()
{
  nh.initNode();

$advertises

}

/* Loop */
void prj_ros_loop()
{
$publications

  nh.spinOnce();
}

#endif